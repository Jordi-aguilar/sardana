.. _sardana-users-guide-index:

============
User's Guide
============

.. toctree::
    :maxdepth: 2
    
    Overview <overview>
    Getting started <getting_started/index>
    Spock <spock>
    Sardana-Taurus Widgets <taurus/index>
    Scans <scan>
    Motion <motion>
    Acquisition <acquisition>
    Diffractometer <diffractometer>
    Adding real elements <adding_elements>
    Macro Hooks <macro_hooks>
    Macro Environment <macro_environment>
    Macro Logging and Reports <macro_logging_and_reports>
    Environment Variable Catalog <environment_variable_catalog>
    Standard macro catalog <standard_macro_catalog>
    Configuration <configuration/index>
    Screenshots <screenshots>
    FAQ <faq>
    Known Problems <https://gitlab.com/sardana-org/sardana/-/issues?scope=all&state=opened&label_name[]=doc:knownbug>
